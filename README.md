# Z-Way Configurator

Configurator is a home automation module for the Z-Way home automation system, enabling the setting of device configuration parameters as a normal automation task. The module lets the user define the parameter number and value and the device it is written to, and creates a virtual device which accomplishes this when activated.

Please note that you should be sure that you know what you're doing before using this module. Do not set undefined, undocumented or reserved parameters. Do not set parameters to undefined or reserved values. Do not set parameters with the wrong size. Use at your own risk, the author takes no responsibility for any harm that should occur to your devices or anything else as a result of using Configurator.

Please also note that battery driven devices will, as a general rule, not have their configuration parameters written immediately, and that initiating a lot of communication which such devices will drain their batteries quicker.

# Configuration

## Physical device ID

The ID number of the physical device you want to configure.

## Parameter number

The number of the parameter to set. You can get this from the device manual or from the device configuration pages in the normal or Expert view. The Expert view is recommended over the normal view, as it gives more information on the accepted values and provides the numeric value of enumerated options as well.

## Parameter value

The value that is to be written to the parameter. Both positive and negative numbers are accepted, but it will be stripped to size.

## Parameter size

The size, in number of bytes, of the parameter to write. This information is not provided in the normal view nor the Expert view, but can usually be found in your device manual (some devices have advanced manuals available online that contain this information). The size can be 1, 2 or 4 bytes. Putting a value of 0 here forces the system to guess based on what was last reported by the device.

# Installation

The preferred way to install this module is through the "Z-Wave.me App Store."

# License

Cog icon based on the cog shape in FontAwesome (http://fontawesome.io).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.