/*** Configurator Z-Way HA module *******************************************
Version: 0.2
-----------------------------------------------------------------------------
Author: Kjetil Volden
Description:
    This module is used for setting device configuration parameters.
******************************************************************************/

// ----------------------------------------------------------------------------
// --- Class definition, inheritance and setup
// ----------------------------------------------------------------------------

function Configurator(id, controller){
    // Call superconstructor first (AutomationModule)
    Configurator.super_.call(this, id, controller);
}

inherits(Configurator, AutomationModule);

_module = Configurator;


// ----------------------------------------------------------------------------
// --- Module instance initialized
// ----------------------------------------------------------------------------

Configurator.prototype.init = function (config){
    Configurator.super_.prototype.init.call(this, config);

    var self = this;

    this.vDev = this.controller.devices.create({
        deviceId: "Configurator_" + this.id,
        defaults: {
            deviceType: "toggleButton",
            metrics: {
                level: 'on', // Always on, but allows bind
                icon: '/ZAutomation/api/v1/load/modulemedia/Configurator/icon.png',
                title: 'Configurator ' + this.id
            }
        },
        overlay: {},
        handler: function (){
            dev_id = self.config.device_id;
            p_num = self.config.param_num;
            p_val = self.config.param_val;
            p_size = self.config.param_size;
            
            zway.devices[dev_id].Configuration.Set(p_num, p_val, p_size);

            self.vDev.set("metrics:level", "on"); // Update to allow catching event
        },
        moduleId: this.id
    });
};

Configurator.prototype.stop = function (){
    if(this.vDev){
        this.controller.devices.remove(this.vDev.id);
        this.vDev = null;
    }
    Configurator.super_.prototype.stop.call(this);
};
